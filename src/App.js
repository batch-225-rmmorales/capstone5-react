import logo from "./logo.svg";
import "./App.css";
import Navbar from "./components/Navbar";
import Drawer from "./components/Drawer";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Login from "./pages/Login";
import Login2 from "./pages/Login2";
import Register from "./pages/Register";
import Sell from "./pages/Sell";
import Edit from "./pages/Edit";
import Order from "./pages/Order";

import Marketplace from "./pages/Marketplace";

export default function App() {
  return (
    <>
      <div className="grid grid-cols-12">
        <div className="col-span-12 sticky top-0 z-50 order-first" id="header">
          <Navbar />
        </div>
        <div className="hidden md:block md:col-span-2" id="menu">
          <Drawer />
        </div>
        <div className="col-span-12 md:col-span-8 bg-base-200" id="body">
          <Router>
            <Routes>
              <Route path="/" element={<Marketplace />} />
              <Route path="/register" element={<Register />} />
              <Route path="/sell" element={<Sell />} />
              <Route path="/edit/:id" element={<Edit />} />
              <Route path="/login" element={<Login />} />
              <Route path="/login2" element={<Login2 />} />
              <Route path="/order" element={<Order />} />
              <Route
                path="/products"
                element={
                  <h1 className="text-3xl font-bold underline">
                    Buy products here
                  </h1>
                }
              />
              <Route
                path="/admin"
                element={
                  <h1 className="text-3xl font-bold underline">
                    Admin Functions Here
                  </h1>
                }
              />
            </Routes>
          </Router>
        </div>
        <div
          className="col-span-12 md:col-span-2 order-first md:order-last"
          id="sidebar"
        ></div>
      </div>
    </>
  );
}
