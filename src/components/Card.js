import Carousel from "./Carousel";
import { useNavigate, Navigate } from "react-router-dom";
import Swal from "sweetalert2";

export default function Card({ item }) {
  const navigate = useNavigate();
  const { thumbnail, title, price, isActive } = item;

  function checkout(event) {
    event.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/product/checkout`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        buyer: localStorage.getItem("email"),
        id: item.id,

        products: [
          {
            id: item.id,
            title: item.title,
            description: item.description,
            category: item.category,
            price: item.price,
            stock: item.stock,
            image: item.thumbnail,
            quantity: 1,
          },
        ],
      }),
    })
      .then((response) => response.json())
      .then((result) => {
        Swal.fire({
          title: result.message,
          icon: "success",
          text: "hooray",
        });
        navigate("/order");
      });
  }
  function toggleActive(event) {
    event.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/product/edit`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        filter: {
          id: item.id,
        },
        update: {
          isActive: !item.isActive,
        },
      }),
    })
      .then((response) => response.json())
      .then((result) => {
        Swal.fire({
          title: result.message,
          icon: "success",
          text: "hooray",
        });
        navigate("/");
      });
  }
  return (
    <>
      <div className="card card-compact bg-base-100 shadow-xl">
        <figure>
          <img
            className="object-cover aspect-[16/9]"
            src={thumbnail}
            alt={title}
          />
        </figure>
        <div className="card-body">
          <p>
            {title}{" "}
            <label htmlFor={"modalItem" + item.id} className="btn btn-xs">
              Buy Now
            </label>
            <span className="btn btn-xs">
              {isActive ? "active" : "inactive"}
            </span>
          </p>
          <h2 className="card-title">PHP {price}</h2>
        </div>
      </div>

      <input
        type="checkbox"
        id={"modalItem" + item.id}
        className="modal-toggle"
      />
      <label htmlFor={"modalItem" + item.id} className="modal">
        <label className="modal-box w-11/12 max-w-5xl" htmlFor="">
          <div className="grid grid-cols-12 w-full">
            <div className="carousel w-full col-span-5">
              <Carousel pics={item.images} itemId={item.id} />
            </div>
            <div className="col-span-7 p-3">
              <article className="prose-lg prose-slate">
                <h3>{item.title}</h3>
                <p>{item.description}</p>
                <p>
                  <strong>PHP {Number(item.price).toLocaleString()}</strong>
                </p>
              </article>
              Seller:
              <span className="badge">
                <div className="dropdown dropdown-hover">
                  {item.seller}
                  <ul
                    tabIndex="-1"
                    className="dropdown-content menu p-2 shadow bg-base-100 text-black rounded-box w-52"
                  >
                    <li>
                      <a href={"/messages?chatwith=" + item.seller}>
                        Chat Seller
                      </a>
                    </li>
                    <li>
                      <a href={"/?seller=" + item.seller} target="_self">
                        View Seller Shop
                      </a>
                    </li>
                  </ul>
                </div>
              </span>
              Category:
              <span className="badge">
                <a href={"/?category=" + item.category} target="_self">
                  {item.category}
                </a>
              </span>
              Stock:
              <span className="badge">{item.stock} items left</span>
              <br />
              <div className="btn-group my-3 ml-3">
                <button
                  className="btn btn-md btn-active"
                  onClick={(event) => checkout(event)}
                >
                  Checkout
                </button>
                <button
                  className="btn btn-md"
                  onClick={() => {
                    navigate("/edit/" + item.id);
                  }}
                  disabled={localStorage.getItem("email") != "admin@admin.com"}
                >
                  Edit Item
                </button>
                <button
                  className="btn btn-md"
                  onClick={toggleActive}
                  disabled={localStorage.getItem("email") != "admin@admin.com"}
                >
                  Archive
                </button>
              </div>
            </div>
          </div>
        </label>
      </label>
    </>
  );
}
