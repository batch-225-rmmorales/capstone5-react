import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import { useNavigate, Navigate } from "react-router-dom";

export default function Navbar() {
  function clearLocal() {
    localStorage.clear();
    window.location.href = "/login";
  }
  let loggedIn;
  if (localStorage.getItem("token")) {
    loggedIn = true;
  } else {
    loggedIn = false;
  }
  return (
    <>
      <div className="navbar bg-base-100 drop-shadow-xl" data-theme="lemonade">
        <div className="flex-none">
          <label htmlFor="my-drawer" className="btn btn-ghost drawer-button">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              fill="none"
              viewBox="0 0 24 24"
              className="inline-block w-5 h-5 stroke-current"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth="2"
                d="M4 6h16M4 12h16M4 18h16"
              />
            </svg>
          </label>
        </div>
        <div className="flex-1">
          <a className="btn btn-ghost normal-case text-xl">🔥 tindero</a>
        </div>
        <div className="flex-none gap-2">
          <div className="form-control">
            <input
              type="text"
              placeholder="Search"
              className="input input-bordered"
            />
          </div>
          <div className="dropdown dropdown-end">
            <label tabIndex="0" className="btn btn-ghost btn-circle avatar">
              <div className="w-10 rounded-full">
                <img src="https://www.looper.com/img/gallery/the-untold-truth-of-v-for-vendetta/l-intro-1645537694.jpg" />
              </div>
            </label>
            <ul
              tabIndex="0"
              className="mt-3 p-2 shadow menu menu-compact dropdown-content bg-base-100 rounded-box w-52"
            >
              <li hidden>
                <a className="justify-between">
                  Profile
                  <span className="badge">New</span>
                </a>
              </li>
              <li hidden={!loggedIn}>
                <a onClick={clearLocal}>Logout</a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </>
  );
}
