import { BrowserRouter as Router, Routes, Route } from "react-router-dom";

export default function Drawer() {
  let loggedIn;
  if (localStorage.getItem("token")) {
    loggedIn = true;
  } else {
    loggedIn = false;
  }
  return (
    <>
      <div className="drawer drawer-mobile bg-base-300" data-theme="lemonade">
        <input id="my-drawer" type="checkbox" className="drawer-toggle" />
        <div className="drawer-side drop-shadow-xl">
          <label htmlFor="my-drawer" className="drawer-overlay">
            x
          </label>
          <ul className="menu p-4">
            <li hidden={loggedIn}>
              <a href="/register">Register</a>
            </li>
            <li hidden={loggedIn}>
              <a href="/login">Login</a>
            </li>
            <li hidden={!loggedIn}>
              <a href="/">Marketplace</a>
            </li>
            <li
              hidden={
                !loggedIn || localStorage.getItem("email") == "admin@admin.com"
              }
            >
              <a href="/order">Order History</a>
            </li>
            <li
              hidden={
                !loggedIn || localStorage.getItem("email") != "admin@admin.com"
              }
            >
              <a href="/sell">Admin: Create Product</a>
            </li>
          </ul>
        </div>
      </div>
    </>
  );
}
