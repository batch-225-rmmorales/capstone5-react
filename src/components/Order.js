export default function Order({ cart }) {
  return (
    <div className="overflow-x-auto w-full p-5">
      <article className="prose">
        <h2>
          Order #{cart.id} <br />
          Purchased on: {cart.updatedAt}
          <br />
        </h2>
      </article>
      <table className="table w-full">
        <thead>
          <tr>
            <th style={{ width: "55%" }}>Product</th>
            <th style={{ width: "55%" }}>Price/Unit</th>
            <th style={{ width: "55%" }}>Quantity</th>
            <th style={{ width: "55%" }}>Price</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <td>
              <div className="flex items-center space-x-3">
                <div className="avatar">
                  <div className="mask mask-squircle w-12 h-12">
                    <img
                      src={cart.products[0].image}
                      alt="Avatar Tailwind CSS Component"
                    />
                  </div>
                </div>
                <div>
                  {cart.products[0].title}
                  <br />
                  <span className="badge badge-ghost badge-sm">
                    Play Games on this console
                  </span>
                </div>
              </div>
            </td>
            <td>{Number(cart.products[0].price).toLocaleString()}</td>
            <td>{Number(cart.products[0].quantity)}</td>
            <td>
              {(
                Number(cart.products[0].price) *
                Number(cart.products[0].quantity)
              ).toLocaleString()}
            </td>
          </tr>

          <tr className="bg-primary font-bold">
            <td>Total </td>
            <td>{cart.totalProducts} Products</td>
            <td>{cart.totalQuantity} Items</td>
            <td>{Number(cart.total).toLocaleString()}</td>
          </tr>
        </tbody>
      </table>
    </div>
  );
}
