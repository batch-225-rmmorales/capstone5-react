import { useEffect, useState } from "react";

export default function Carousel({ pics, itemId }) {
  const [images, setImages] = useState(pics);
  useEffect(() => {
    setImages(
      images.map((image, index) => {
        return (
          <div
            key={index}
            id={"slide_" + itemId + "_" + Number(index)}
            className="carousel-item relative w-full"
          >
            <img src={image} className="w-full" alt="..." />
            <div className="absolute flex justify-between transform -translate-y-1/2 left-5 right-5 top-1/2">
              <a
                href={"#slide_" + itemId + "_" + (Number(index) - 1)}
                className="btn btn-circle"
              >
                ❮
              </a>
              <a
                href={"#slide_" + itemId + "_" + (Number(index) + 1)}
                className="btn btn-circle"
              >
                ❯
              </a>
            </div>
          </div>
        );
      })
    );
  }, []);

  return (
    <>
      <div className="carousel w-full col-span-5">{images}</div>
    </>
  );
}
