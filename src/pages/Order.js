import Loading from "../components/Loading";
import { useEffect, useState } from "react";
import Order from "../components/Order";

export default function Marketplace() {
  const [orders, setOrders] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  useEffect((isLoading) => {
    // If it detects the data coming from fetch, the set isloading going to be false
    fetch(`${process.env.REACT_APP_API_URL}/product/orders`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((response) => response.json())
      .then((result) => {
        console.log(result);
        result.reverse();
        setOrders(
          result
            .map((cart, index) => {
              return (
                <div
                  key={index}
                  className="col-span-6 sm:col-span-4 lg:col-span-3 m-1"
                >
                  <Order key={index} cart={cart} />
                </div>
              );
            })
            .slice((1 - 1) * 16, 1 * 16)
        );

        // Sets the loading state to false
        setIsLoading(false);
      });
  }, []);

  return isLoading ? <Loading /> : <>{orders}</>;
}
