import { useState, useEffect, useContext, useRef } from "react";
import { useNavigate, Navigate } from "react-router-dom";
import Swal from "sweetalert2";
import UserContext from "../UserContext";

export default function Sell() {
  const titleRef = useRef();
  const descRef = useRef();
  const categoryRef = useRef();
  const thumbnailRef = useRef();
  const priceRef = useRef();
  const stockRef = useRef();

  function createProduct(event) {
    event.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/product/sell`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        title: titleRef.current.value,
        description: descRef.current.value,
        category: categoryRef.current.value,
        thumbnail: thumbnailRef.current.value,
        price: priceRef.current.value,
        stock: stockRef.current.value,
        images: [thumbnailRef.current.value],
        seller: "admin",
      }),
    })
      .then((response) => response.json())
      .then((result) => {
        Swal.fire({
          title: result.message,
          icon: "success",
          text: "hooray",
        });
      });
  }

  return (
    <>
      <div className="d-flex align-items-center container vh-100 align-items-center justify-content-center	">
        <div className="col-md-7 mx-auto text-center">
          <h1 className="mb-6 mb-5">Sell your item at 🔥tindero</h1>
          <div className="row">
            <div className="col-4"></div>
            <div className="col-8">
              <form className="content text-center" method="POST">
                <input
                  className="form-control m-1"
                  size="20"
                  type="text"
                  name="name"
                  placeholder="item name"
                  ref={titleRef}
                />
                <textarea
                  className="form-control m-1"
                  size="20"
                  rows="4"
                  type="text"
                  name="description"
                  placeholder="type description here"
                  ref={descRef}
                />
                <input
                  className="form-control m-1"
                  size="20"
                  type="url"
                  name="category"
                  placeholder="category"
                  ref={categoryRef}
                />
                <input
                  className="form-control m-1"
                  size="20"
                  type="url"
                  name="imgUrl"
                  placeholder="image url here"
                  ref={thumbnailRef}
                />

                <input
                  className="form-control m-1"
                  size="20"
                  type="number"
                  name="price"
                  placeholder="price in PHP"
                  ref={priceRef}
                />
                <input
                  className="form-control m-1"
                  size="20"
                  type="number"
                  name="stock"
                  placeholder="inventory"
                  ref={stockRef}
                />

                <button
                  className="btn btn-primary btn-md mb-5 w-100"
                  onClick={(event) => createProduct(event)}
                >
                  Post Item
                </button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
