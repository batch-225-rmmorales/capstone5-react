import { useState, useEffect, useContext } from "react";
// import { Form, Button } from "react-bootstrap";
import { useNavigate, Navigate } from "react-router-dom";
import UserContext from "../UserContext";
import Swal from "sweetalert2";

export default function Login() {
  // Initializes the use of the properties from the UserProvider in App.js file
  const user = useContext(UserContext);
  const navigate = useNavigate();

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  // Initialize useNavigate
  // const navigate = useNavigate()

  // For determining if button is disabled or not
  const [isActive, setIsActive] = useState(false);
  console.log(process.env.REACT_APP_API_URL);

  function authenticate(event) {
    event.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/user/login`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        email: email,
        password: password,
      }),
    })
      .then((response) => response.json())
      .then((result) => {
        if (typeof result.token !== "undefined") {
          localStorage.setItem("token", result.token);
          localStorage.setItem("email", result.email);
        }
        Swal.fire({
          title: result.message,
          icon: "success",
          text:
            (result.error ? result.error : "") +
            (result.token ? result.token : ""),
        });
        window.location.href = "/";
      });
  }

  useEffect(() => {
    if (email !== "" && password !== "") {
      // Enables the submit button if the form data has been verified
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [email, password]);

  return (
    <div className="hero min-h-screen bg-base-200">
      <div className="hero-content flex-col lg:flex-row-reverse">
        <div className="text-center lg:text-left">
          <h1 className="text-5xl font-bold">Login now!</h1>
          <p className="py-6">
            Provident cupiditate voluptatem et in. Quaerat fugiat ut assumenda
            excepturi exercitationem quasi. In deleniti eaque aut repudiandae et
            a id nisi.
          </p>
        </div>
        <div className="card flex-shrink-0 w-full max-w-sm shadow-2xl bg-base-100">
          <div className="card-body">
            <div className="form-control">
              <label className="label">
                <span className="label-text">Email</span>
              </label>
              <input
                type="text"
                placeholder="email"
                className="input input-bordered"
                value={email}
                onChange={(event) => setEmail(event.target.value)}
                required
              />
            </div>
            <div className="form-control">
              <label className="label">
                <span className="label-text">Password</span>
              </label>
              <input
                type="text"
                placeholder="password"
                className="input input-bordered"
                value={password}
                onChange={(event) => setPassword(event.target.value)}
                required
              />
            </div>
            <div className="form-control mt-6">
              <button
                className="btn btn-primary"
                onClick={(event) => authenticate(event)}
                disabled={!isActive}
              >
                Login
              </button>
            </div>
            <label className="label">
              <span className="label-text-alt">
                Don't have an account?{" "}
                <a href="/register" className="label-text-alt link link-hover">
                  Register here
                </a>
              </span>
            </label>
          </div>
        </div>
      </div>
    </div>
  );
}
