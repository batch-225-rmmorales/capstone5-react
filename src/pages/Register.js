import { useState, useEffect, useContext } from "react";
import { useNavigate, Navigate } from "react-router-dom";
import Swal from "sweetalert2";
import UserContext from "../UserContext";

export default function Register() {
  // const { user, setUser } = useContext(UserContext);
  const navigate = useNavigate();

  const [username, setUsername] = useState("");

  const [email, setEmail] = useState("");
  const [password1, setPassword1] = useState("");

  const [isActive, setIsActive] = useState(false);

  function registerUser(event) {
    event.preventDefault();
    console.log("im trying to register");

    fetch(`${process.env.REACT_APP_API_URL}/user/register`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        username: username,
        email: email,
        password: password1,
      }),
    })
      .then((response) => response.json())
      .then((result) => {
        console.log(result);

        setEmail("");
        setPassword1("");

        if (result) {
          Swal.fire({
            title: "Register Successful!",
            icon: "success",
            text: "Salamat sa pag-rehistro!",
          });

          navigate("/login");
        } else {
          Swal.fire({
            title: "Registration Failed",
            icon: "error",
            text: "Sorry Something errors in your submission :(",
          });
        }
      });
  }

  useEffect(() => {
    if (username !== "" && email !== "" && password1 !== "") {
      // Enables the submit button if the form data has been verified
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [username, email, password1]);

  return (
    <div className="hero min-h-screen bg-base-200">
      <div className="hero-content flex-col lg:flex-row-reverse">
        <div className="text-center lg:text-left">
          <h1 className="text-5xl font-bold">Register Now!</h1>
          <p className="py-6">
            Provident cupiditate voluptatem et in. Quaerat fugiat ut assumenda
            excepturi exercitationem quasi. In deleniti eaque aut repudiandae et
            a id nisi.
          </p>
        </div>
        <div className="card flex-shrink-0 w-full max-w-sm shadow-2xl bg-base-100">
          <div className="card-body">
            <div className="form-control">
              <label className="label">
                <span className="label-text">Username</span>
              </label>
              <input
                type="text"
                placeholder="username"
                className="input input-bordered"
                value={username}
                onChange={(event) => setUsername(event.target.value)}
                required
              />
            </div>
            <div className="form-control">
              <label className="label">
                <span className="label-text">Email</span>
              </label>
              <input
                type="text"
                placeholder="email"
                className="input input-bordered"
                value={email}
                onChange={(event) => setEmail(event.target.value)}
                required
              />
            </div>
            <div className="form-control">
              <label className="label">
                <span className="label-text">Password</span>
              </label>
              <input
                type="password"
                placeholder="password"
                className="input input-bordered"
                value={password1}
                onChange={(event) => setPassword1(event.target.value)}
                required
              />
            </div>
            <div className="form-control mt-6">
              <button
                className="btn btn-primary"
                onClick={(event) => registerUser(event)}
                disabled={!isActive}
              >
                Register
              </button>
            </div>
            <label className="label">
              <span className="label-text-alt">
                Already have an account?{" "}
                <a href="/login" className="label-text-alt link link-hover">
                  Login here
                </a>
              </span>
            </label>
          </div>
        </div>
      </div>
    </div>
  );
}
