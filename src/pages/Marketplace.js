// import CourseCard from '../components/CourseCard'
import Loading from "../components/Loading";
import { useEffect, useState } from "react";
import Card from "../components/Card";

export default function Marketplace() {
  const [marketplace, setMarketplace] = useState([]);
  const [isLoading, setIsLoading] = useState(true);

  useEffect((isLoading) => {
    // If it detects the data coming from fetch, the set isloading going to be false
    fetch(`${process.env.REACT_APP_API_URL}/product/getagain`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((response) => response.json())
      .then((result) => {
        console.log(result);
        result.data.reverse();
        setMarketplace(
          result.data
            .map((item, index) => {
              return (
                <div
                  key={index}
                  className="col-span-6 sm:col-span-4 lg:col-span-3 m-1"
                >
                  <Card key={index} item={item} />
                </div>
              );
            })
            .slice((1 - 1) * 16, 1 * 16)
        );

        // Sets the loading state to false
        setIsLoading(false);
      });
  }, []);

  return isLoading ? (
    <Loading />
  ) : (
    <>
      <div className="grid grid-cols-12 bg-base-200">{marketplace}</div>
    </>
  );
}
