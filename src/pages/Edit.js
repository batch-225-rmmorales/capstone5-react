import {
  useState,
  useEffect,
  useContext,
  useRef,
  useSearchParams,
} from "react";
import { useNavigate, Navigate, useParams } from "react-router-dom";
import Swal from "sweetalert2";
import UserContext from "../UserContext";
import Loading from "../components/Loading";

export default function Sell() {
  const titleRef = useRef();
  const descRef = useRef();
  const categoryRef = useRef();
  const thumbnailRef = useRef();
  const priceRef = useRef();
  const stockRef = useRef();
  const [title, setTitle] = useState();
  const [description, setDescription] = useState();
  const [category, setCategory] = useState();
  const [thumbnail, setThumbnail] = useState();
  const [price, setPrice] = useState();
  const [stock, setStock] = useState();
  const [isLoading, setIsLoading] = useState(true);
  // const [searchParams, setSearchParams] = useSearchParams();
  const params = useParams();

  useEffect((isLoading) => {
    // If it detects the data coming from fetch, the set isloading going to be false
    console.log("params");

    console.log(params);
    fetch(
      `${process.env.REACT_APP_API_URL}/product/getagain?item=` + params.id,
      {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
      }
    )
      .then((response) => response.json())
      .then((result) => {
        setTitle(result.data.title);
        setDescription(result.data.description);
        setCategory(result.data.category);
        setThumbnail(result.data.thumbnail);
        setPrice(result.data.price);
        setStock(result.data.stock);

        // Sets the loading state to false
        console.log(result);
        setIsLoading(false);
      });
  }, []);

  function updateProduct(event) {
    event.preventDefault();

    fetch(`${process.env.REACT_APP_API_URL}/product/edit`, {
      method: "POST",
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
      body: JSON.stringify({
        filter: {
          id: params.id,
        },
        update: {
          title: titleRef.current.value,
          description: descRef.current.value,
          category: categoryRef.current.value,
          thumbnail: thumbnailRef.current.value,
          price: priceRef.current.value,
          stock: stockRef.current.value,
          images: [thumbnailRef.current.value],
          seller: "admin",
        },
      }),
    })
      .then((response) => response.json())
      .then((result) => {
        Swal.fire({
          title: result.message,
          icon: "success",
          text: "hooray",
        });
      });
  }

  return isLoading ? (
    <Loading />
  ) : (
    <>
      <div className="d-flex align-items-center container vh-100 align-items-center justify-content-center	">
        <div className="col-md-7 mx-auto text-center">
          <h1 className="mb-6 mb-5">Sell your item at 🔥tindero</h1>
          <div className="row">
            <div className="col-4"></div>
            <div className="col-8">
              <form className="content text-center" method="POST">
                <input
                  className="form-control m-1"
                  size="20"
                  type="text"
                  name="name"
                  placeholder="item name"
                  defaultValue={title}
                  ref={titleRef}
                />
                <textarea
                  className="form-control m-1"
                  size="20"
                  rows="4"
                  type="text"
                  name="description"
                  placeholder="type description here"
                  defaultValue={description}
                  ref={descRef}
                />
                <input
                  className="form-control m-1"
                  size="20"
                  type="url"
                  name="category"
                  placeholder="category"
                  defaultValue={category}
                  ref={categoryRef}
                />
                <input
                  className="form-control m-1"
                  size="20"
                  type="url"
                  name="imgUrl"
                  placeholder="image url here"
                  defaultValue={thumbnail}
                  ref={thumbnailRef}
                />

                <input
                  className="form-control m-1"
                  size="20"
                  type="number"
                  name="price"
                  placeholder="price in PHP"
                  defaultValue={price}
                  ref={priceRef}
                />
                <input
                  className="form-control m-1"
                  size="20"
                  type="number"
                  name="stock"
                  placeholder="inventory"
                  defaultValue={stock}
                  ref={stockRef}
                />

                <button
                  className="btn btn-primary btn-md mb-5 w-100"
                  onClick={(event) => updateProduct(event)}
                >
                  Update Item
                </button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
